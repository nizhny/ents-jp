from data_preprocess import process_raw_data, editnet_data_to_editnetID

with open("./jpdata/pos_set.txt.jp",'r') as f:
    tagdict = f.read().split("\n")

dname = "jpdata/"

with open(dname+"train.comp.parse", "r") as f:
    comp_txt = f.read().split("\n")[:-1]

with open(dname+"train.simp.parse", "r") as f:
    simp_txt = f.read().split("\n")[:-1]

with open(dname+"train.comp.postag", "r") as f:
    pos_txt = f.read().split("\n")[:-1]

df = process_raw_data(comp_txt, simp_txt, pos_txt, "./jpdata/")

editnet_data_to_editnetID(df, "traindata/train.df.filtered.pos")

import pandas as pd
pd.read_pickle("traindata/train.df.filtered.pos").head()

dname = "jpdata/"

with open(dname+"valid.comp.parse", "r") as f:
    comp_txt = f.read().split("\n")[:-1]

with open(dname+"valid.simp.parse", "r") as f:
    simp_txt = f.read().split("\n")[:-1]

with open(dname+"valid.comp.postag", "r") as f:
    pos_txt = f.read().split("\n")[:-1]

df = process_raw_data(comp_txt, simp_txt, pos_txt, "./jpdata/")

editnet_data_to_editnetID(df, "traindata/val.df.filtered.pos")

!python3 main.py 

import torch
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(device)

from evaluator import Evaluator
import data
import torch.nn as nn

eval_dataset = data.Dataset('/host/YASASII/EditNTS/traindata/val.df.filtered.pos') # load eval dataset
evaluator = Evaluator(loss= nn.NLLLoss(ignore_index=vocab.w2i['PAD'], reduction='none'))

val_loss, bleu_score, sari, sys_out = evaluator.evaluate(eval_dataset, vocab, edit_net,args, epoch)
log_msg = "epoch %d, step %d, Dev loss: %.4f, Bleu score: %.4f, Sari: %.4f \n" % (epoch, i, val_loss, bleu_score, sari)